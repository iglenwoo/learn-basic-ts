// let & const <- ECMAScript 2015 (ES6)
let shouldUseLet: boolean = true;
const shouldUseConst: boolean = true;
// ES6: Arrow Function, Classes, Default Parameters
const arrowFunction = () => {};



// All numbers are floating point value just like javascript
let decimal: number = 6;
// TypeScript supports binary and octal literals introduced in ECMAScript 2015
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;


// String templation
const inputStr: string = 'abc';
const templatedStr: string = `Take input as ${inputStr}`;
console.log('-- String --');
console.log(templatedStr);
console.log();


// Array
// way 1
let list1 : number[] = [1, 2, 3];
// way 2
let list2: Array<number> = [1, 2, 3];
console.log('-- Array --');
console.log(list1);
console.log(list2);
console.log();


// Tuple
let x: [string, number];
x = ['hi', 5];
//x = [2, 2]; // Error
console.log('-- Tuple --');
console.log(x);
console.log();


// Enum
enum Color {Red, Green, Blue}
let c:Color = Color.Blue;
console.log('-- Enum --');
console.log(c);
console.log();


// Any
let notSure: any = 4;
console.log('-- Any --');
console.log(notSure);
notSure = "maybe a string instead";
console.log(notSure);
notSure = false; // okay, definitely a boolean
console.log(notSure);
console.log();


// Object
declare function create(o: object | null): void;

create({ prop: 0 }); // OK
create(null); // OK

//create(42); // Error
//create("string"); // Error
//create(false); // Error
create(undefined); // Error
