//https://facebook.github.io/create-react-app/
//https://github.com/facebook/create-react-app#creating-an-app

//You’ll need to have Node 8.16.0 or Node 10.16.0 or later version on your local development machine
npx create-react-app react-js
npx create-react-app react-ts --scripts-version=react-scripts-ts
