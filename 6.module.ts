import { ZipCodeValidator } from "./ZipCodeValidator";
import { ZipCodeValidator as ZCV } from "./ZipCodeValidator";
import * as validator from "./ZipCodeValidator";

let myValidator = new ZipCodeValidator();
let renamedValidator = new ZCV();
let entireValidator = new validator.ZipCodeValidator();